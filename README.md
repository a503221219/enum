1. 创建枚举文件
```php
<?php
use Enum\Enum;
class UserType extends Enum{
    public const USER = 1;
    public const VIP = 2;
    public static $label = [
        self::USER => '会员',
        self::VIP => '超级会员'
    ];
}
```
2. 获取所有（键值对）
```php
\UserType::asSelectArray();
```
3. 获取label
```php
\UserType::getLabel($value);
```
4. 判断是否包含
```php
\UserType::in($needle, $haystack = null);
```