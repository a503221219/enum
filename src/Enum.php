<?php

namespace Enum;

/*
 * @Description: 枚举
 * @Version: 1.0
 * @Autor: lzy
 * @Date: 2022-07-02 17:55:21
 * @LastEditors: lzy
 * @LastEditTime: 2022-07-02 19:48:10
 * @Motto: follow your heart
 */

abstract class Enum
{
    private static $obj_class = null;
    private static $all_enums = null;
    private static $label = null;
    /**
     * 获取所有
     *
     * @Author 梁泽亚
     * @DateTime 2022-07-02
     * @param [type] $type enums labels
     * @return void
     */
    public static function getAll($type = null)
    {
        if (self::$all_enums === null) {
            self::$obj_class = new \ReflectionClass(get_called_class());
            self::$all_enums = self::$obj_class->getConstants();
            self::$label = self::$obj_class->getStaticPropertyValue('label');
        }
        switch ($type) {
            case 'enums':
                return self::$all_enums;
                break;
            case 'labels':
                return self::$label;
                break;
            default:
                return ['enums' => self::$all_enums, 'labels' => self::$label];
                break;
        }
    }
    /**
     * 获取select
     *
     * @Author 梁泽亚
     * @DateTime 2022-07-02
     * @return void
     */
    public static function asSelectArray()
    {
        $data = (array)self::getAll('enums');
        $data = array_flip($data);
        foreach ($data as $key => &$value) {
            $value = self::$label[$key];
        }
        return $data;
    }
    /**
     * 获取label
     *
     * @Author 梁泽亚
     * @DateTime 2022-07-02
     * @param [type] $value
     * @return void
     */
    public static function getLabel($value)
    {
        $data = self::getAll('labels');
        return $data[$value] ?? '';
    }
    /**
     * 判断是否包含
     *
     * @Author 梁泽亚
     * @DateTime 2022-07-02
     * @param [type] $needle
     * @param [type] $haystack
     * @return boolean
     */
    public static function in($needle, $haystack = null)
    {
        $data = self::getAll('enums');
        if ($haystack === null) {
            $haystack = $data;
        }
        return in_array($needle, $haystack);
    }
}
